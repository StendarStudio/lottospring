package Stendar.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @ResponseBody
    @GetMapping("/") // po tej sciezce wywolana jest metoda hello
    public String hello() {
        return "Witaj świecie";
    }

    @ResponseBody
    @GetMapping("/bye")

    public String bye() {
        return "Baj baj";
    }



    @GetMapping("/welcome")
    public String welcome() {
        return "witam"; // ma zwrocic html
        //resources/templates.witam.html
    }

    @GetMapping("/product")
    public String product () {
        return "pralka";
    }

    @GetMapping("/lotto")
    public String generateLotto(ModelMap map) {

        LottoGenerator lottoGenerator = new LottoGenerator();

        map.put(("numbers"), lottoGenerator.generate());

        return "lotto";
    }
}

